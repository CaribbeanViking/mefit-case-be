﻿using mefit_case_be.Model;
using Microsoft.EntityFrameworkCore;

namespace mefit_case_be.Data
{
    public class MeFitDBContext : DbContext
    {
        public MeFitDBContext(DbContextOptions options)
            : base(options)
        {

        }

        public DbSet<Plan> Plans { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Stats> Stats { get; set; }

        /// <summary>
        /// Fills the database with dummydata.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Plan>().HasData(InitialData.GetPlans());
            modelBuilder.Entity<Workout>().HasData(InitialData.GetWorkouts());
            modelBuilder.Entity<Exercise>().HasData(InitialData.GetExercises());
            modelBuilder.Entity<User>().HasData(InitialData.GetUsers());
            modelBuilder.Entity<Account>().HasData(InitialData.GetAccountData());
            modelBuilder.Entity<Address>().HasData(InitialData.GetAddressData());
            modelBuilder.Entity<Stats>().HasData(InitialData.GetStatsData());

            modelBuilder.Entity<Plan>()
                .HasMany(p => p.Workouts) // MANY-TO-MANY
                .WithMany(w => w.Plans) // MANY-TO-MANY
                .UsingEntity<Dictionary<string, object>>(
                    "PlanWorkout", // Join table
                    r => r.HasOne<Workout>().WithMany().HasForeignKey("WorkoutId"), // Right table
                    l => l.HasOne<Plan>().WithMany().HasForeignKey("PlanId"), // Left table
                    je =>
                    {
                        je.HasKey("WorkoutId", "PlanId");
                        je.HasData( // Specify what Plan belongs to what Workout
                            new { PlanId = 1, WorkoutId = 5 },
                            new { PlanId = 1, WorkoutId = 8 },
                            new { PlanId = 1, WorkoutId = 3 },

                            new { PlanId = 2, WorkoutId = 1 },
                            new { PlanId = 2, WorkoutId = 2 },
                            new { PlanId = 2, WorkoutId = 3 },

                            new { PlanId = 3, WorkoutId = 4 },
                            new { PlanId = 3, WorkoutId = 6 },
                            new { PlanId = 3, WorkoutId = 8 },

                            new { PlanId = 4, WorkoutId = 7 },
                            new { PlanId = 4, WorkoutId = 6 },
                            new { PlanId = 4, WorkoutId = 4 }
                        );
                    });

            modelBuilder.Entity<Workout>()
               .HasMany(w => w.Exercises) // MANY-TO-MANY
               .WithMany(e => e.Workouts) // MANY-TO-MANY
               .UsingEntity<Dictionary<string, object>>(
                   "WorkoutExercise", // Join table
                   r => r.HasOne<Exercise>().WithMany().HasForeignKey("ExerciseId"), // Right table
                   l => l.HasOne<Workout>().WithMany().HasForeignKey("WorkoutId"), // Left table
                   je =>
                   {
                       je.HasKey("ExerciseId", "WorkoutId");
                       je.HasData( // Specify what Plan belongs to what Workout
                           new { WorkoutId = 1, ExerciseId = 6 },
                           new { WorkoutId = 1, ExerciseId = 9 },
                           new { WorkoutId = 1, ExerciseId = 14 },

                           new { WorkoutId = 2, ExerciseId = 6 },
                           new { WorkoutId = 2, ExerciseId = 9 },
                           new { WorkoutId = 2, ExerciseId = 10 },

                           new { WorkoutId = 3, ExerciseId = 3 },
                           new { WorkoutId = 3, ExerciseId = 6 },
                           new { WorkoutId = 3, ExerciseId = 14 },

                           new { WorkoutId = 4, ExerciseId = 2 },
                           new { WorkoutId = 4, ExerciseId = 4 },
                           new { WorkoutId = 4, ExerciseId = 5 },

                           new { WorkoutId = 5, ExerciseId = 1 },
                           new { WorkoutId = 5, ExerciseId = 7 },
                           new { WorkoutId = 5, ExerciseId = 8 },

                           new { WorkoutId = 6, ExerciseId = 11 },
                           new { WorkoutId = 6, ExerciseId = 12 },
                           new { WorkoutId = 6, ExerciseId = 13 },

                           new { WorkoutId = 7, ExerciseId = 15 },
                           new { WorkoutId = 7, ExerciseId = 16 },
                           new { WorkoutId = 7, ExerciseId = 17 },

                           new { WorkoutId = 8, ExerciseId = 1 },
                           new { WorkoutId = 8, ExerciseId = 2 },
                           new { WorkoutId = 8, ExerciseId = 3 }
                       );
                   });
        }
    }
}

﻿using mefit_case_be.Model;

namespace mefit_case_be.Data
{
    /// <summary>
    /// InitialData populates tables with dummy data.
    /// </summary>
    public class InitialData
    {
        public static IEnumerable<Plan> GetPlans()
        {
            IEnumerable<Plan> Plans = new List<Plan>()
            {
                new Plan {
                    Id = 1,
                    Name = "Athletic",
                    Description = "For Real Athletes!"
                },

                new Plan {
                    Id = 2,
                    Name = "Strength",
                    Description = "Heavy Training for Arnold Wannabees!"
                },

                new Plan {
                    Id = 3,
                    Name = "Cardio",
                    Description = "Live Fast, Die Old!"
                },

                new Plan {
                    Id = 4,
                    Name = "Mobility",
                    Description = "Dance Like a Butterfly, Sting Like a Bee!"
                }
            };
            return Plans;
        }
        public static IEnumerable<Workout> GetWorkouts()
        {
            IEnumerable<Workout> Workouts = new List<Workout>()
            {
                new Workout {
                    Id = 1,
                    Name = "Weights",
                    Description = "Classic Heavy Indoor Gym Training",
                    Is_Complete = false
                },

                new Workout {
                    Id = 2,
                    Name = "Cords",
                    Description = "Rubber Cord Training",
                    Is_Complete = false
                },

                new Workout {
                    Id = 3,
                    Name = "Bodypump",
                    Description = "Step and Barbell Training",
                    Is_Complete = false
                },

                new Workout {
                    Id = 4,
                    Name = "Running",
                    Description = "Treadmill Training",
                    Is_Complete = false
                },

                new Workout {
                    Id = 5,
                    Name = "Boxing",
                    Description = "Indoor Pair Boxing Training",
                    Is_Complete = false
                },

                new Workout {
                    Id = 6,
                    Name = "Swimming",
                    Description = "Cardio Lap Training",
                    Is_Complete = false
                },

                new Workout {
                    Id = 7,
                    Name = "Yoga",
                    Description = "Mindfullnes, Balance and Mobility Training",
                    Is_Complete = false
                },

                new Workout {
                    Id = 8,
                    Name = "HIIT",
                    Description = "High Intensity Interval Training",
                    Is_Complete = false
                },

            };
            return Workouts;
        }
        public static IEnumerable<Exercise> GetExercises()
        {
            IEnumerable<Exercise> Exercises = new List<Exercise>()
            {
                new Exercise {
                    Id = 1,
                    Name = "Jump Rope",
                    Description = "3 x 5 Minutes",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 2,
                    Name = "Interval Lapping",
                    Description = "10 x 1 Minute",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 3,
                    Name = "Box Jumps",
                    Description = "10 x 5 Reps.",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 4,
                    Name = "Jogging",
                    Description = "5 km",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 5,
                    Name = "Hill Scaling",
                    Description = "5 Reps.",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 6,
                    Name = "Leg Bends",
                    Description = "3 x 10 Reps.",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 7,
                    Name = "Jabbing",
                    Description = "5 x 20 Reps.",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 8,
                    Name = "Uppercuts",
                    Description = "5 x 20 Reps.",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 9,
                    Name = "Bicep Curls",
                    Description = "3 x 10 Reps.",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 10,
                    Name = "Shoulder Cross",
                    Description = "3 x 12 Reps.",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 11,
                    Name = "Freestyle",
                    Description = "10 Laps",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 12,
                    Name = "Butterfly",
                    Description = "10 Laps",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 13,
                    Name = "Backstrokes",
                    Description = "10 Laps",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 14,
                    Name = "Chest Press",
                    Description = "3 x 12 Reps.",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 15,
                    Name = "Child Position",
                    Description = "5 Minutes",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 16,
                    Name = "Dog Position",
                    Description = "5 Minutes",
                    Is_Complete = false
                },

                new Exercise {
                    Id = 17,
                    Name = "Warrior Position",
                    Description = "5 Minutes",
                    Is_Complete = false
                },
            };
            return Exercises;
        }
        public static IEnumerable<User> GetUsers()
        {
            IEnumerable<User> Users = new List<User>()
            {
                new User {
                    Id = 1,
                    First_Name = "Arnold",
                    Last_Name = "Schwarzenegger",
                    Email = "admin",
                    Password = "admin",
                    Is_Admin = true,
                    Is_Contributor = false,
                }
            };
            return Users;
        }

        public static IEnumerable<Account> GetAccountData()
        {
            IEnumerable<Account> AccountData = new List<Account>()
            {
                new Account {
                    Id = 1,
                    Weight =  "75",
                    Height = "180",
                    Medical_Conditions = "Diabetes",
                    Disabilities = "Knee injury",
                    UserID = 1,
                    AddressID = 1,
                    StatsID = 1,
                    PlanID = null,
                }
            };
            return AccountData;
        }

        public static IEnumerable<Address> GetAddressData()
        {
            IEnumerable<Address> AddressData = new List<Address>()
            {
                new Address {
                    Id = 1,
                    Address_1 =  "Storgatan 12",
                    Postal_Code = "541 46",
                    City = "Skövde",
                    Country = "Sweden"
                }
            };
            return AddressData;
        }

        public static IEnumerable<Stats> GetStatsData()
        {
            IEnumerable<Stats> StatsData = new List<Stats>()
            {
                new Stats {
                    Id = 1,
                    Button_1 =  false,
                    Button_2 = false,
                    Button_3 = false,
                }
            };
            return StatsData;
        }
    }
}



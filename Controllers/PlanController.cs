﻿using AutoMapper;
using mefit_case_be.Data;
using mefit_case_be.DTO.PlanDTOs;
using mefit_case_be.DTO.WorkoutDTOs;
using mefit_case_be.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace mefit_case_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlanController : ControllerBase
    {
        // Dependency Injections
        private readonly MeFitDBContext _context;
        private readonly IMapper _mapper;

        // Constructor
        public PlanController(MeFitDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
                                        // ---------------------------------------- //
                    // ----------------------------------------------------------------------------------- //
        // -------------------------------------- // CONTROLLERS/ENDPOINTS \\ ------------------------------------------ //
                    // ----------------------------------------------------------------------------------- //
                                        // ---------------------------------------- //


        // GET: api/<PlanController>
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<ReadPlanDTO>>> GetAllPlans()
        {
            return _mapper.Map<List<ReadPlanDTO>>(await _context.Plans
                .Include(p => p.Workouts)
                .ToListAsync());
        }

        // GET api/<PlanController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadPlanDTO>> GetPlanById(int id)
        {
            Plan plan = await _context.Plans
                .Include(p => p.Workouts)
                .Where(p => p.Id == id)
                .FirstAsync();

            if(plan == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadPlanDTO>(plan);
        }

        /// <summary>
        /// Gets all the workouts that have a relationship with a specified plan by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/Plan/Id/Workouts
        [HttpGet("{id}/workouts/all")]
        public async Task<ActionResult<List<ReadWorkoutDTO>>> GetWorkoutsByPlanId(int id)
        {
            return _mapper.Map<List<ReadWorkoutDTO>>(await _context.Workouts.Include(m => m.Exercises)
               .Where(c => c.Plans.Select(i => i.Id).Contains(id)).ToListAsync());
        }

        // POST api/<PlanController>/5
        [HttpPost]
        public async Task<ActionResult> AddNewPlan([FromBody] CreatePlanDTO plan)
        {
            var domainPlan = _mapper.Map<Plan>(plan);
            _context.Plans.Add(domainPlan);
            await _context.SaveChangesAsync();
            return Ok();
        }

        // PUT api/<PlanController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> PutPlanById(int id, [FromBody] UpdatePlanDTO plan)
        {

            //var domainPlan = GetPlanById(id);
            var domainPlan = _mapper.Map<Plan>(plan);
            
            _context.Entry(domainPlan).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return NoContent();
        }

        /// <summary>
        /// This endpoint is deprecated due to a re-structure and functionality change. 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="patch"></param>
        /// <returns></returns>
        // PATCH api/<PlanController>/5
        [HttpPatch("{id}")]
        public async Task<ActionResult> PatchPlanById(int id, [FromBody] UpdatePlanDTO patch)
        {
            var fromDb = _context.Plans.FirstOrDefault(p => p.Id == id);

            fromDb.Is_Complete = patch.Is_Complete;

            _context.Update(fromDb);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE api/<PlanController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePlanById(int id)
        {
            var plan = _context.Plans.Find(id);
            if (plan == null)
            {
                return NotFound();
            }

            _context.Plans.Remove(plan);
            await _context.SaveChangesAsync();

            return NoContent();
        }

    }
}

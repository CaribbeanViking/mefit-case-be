﻿using AutoMapper;
using mefit_case_be.Data;
using mefit_case_be.DTO.AddressDTOs;
using mefit_case_be.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace mefit_case_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        // Dependency Injections
        private readonly MeFitDBContext _context;
        private readonly IMapper _mapper;

        // Constructor
        public AddressController(MeFitDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
                                            // ---------------------------------------- //
                        // ----------------------------------------------------------------------------------- //
        // -------------------------------------- // CONTROLLERS/ENDPOINTS \\ ------------------------------------------ //
                        // ----------------------------------------------------------------------------------- //
                                           // ---------------------------------------- //

        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<ReadAddressDTO>>> GetAllAddresses()
        {
            return _mapper.Map<List<ReadAddressDTO>>(await _context.Addresses.ToListAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ReadAddressDTO>> GetAddressById(int id)
        {
            var domainAddress = await _context.Addresses.FindAsync(id);

            if (domainAddress == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadAddressDTO>(domainAddress);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateAddress([FromBody] UpdateAddressDTO updateAddress)
        {
            var domainAddress = _mapper.Map<Address>(updateAddress);

            _context.Entry(domainAddress).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // POST api/<PlanController>/5
        [HttpPost]
        public async Task<ActionResult> AddNewAddress([FromBody] CreateAddressDTO address)
        {
            var domainAddress = _mapper.Map<Address>(address);
            _context.Addresses.Add(domainAddress);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAddress(int id)
        {
            var address = _context.Addresses.Find(id);
            if (address == null)
            {
                return NotFound();
            }

            _context.Addresses.Remove(address);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
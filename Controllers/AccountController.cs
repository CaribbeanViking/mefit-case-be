﻿using AutoMapper;
using mefit_case_be.Data;
using mefit_case_be.DTO.AccountDTOs;
using mefit_case_be.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace mefit_case_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        // Dependency Injections
        private readonly MeFitDBContext _context;
        private readonly IMapper _mapper;

        // Constructor
        // Sets the local context to the same as the database context in order to access the DbSets
        public AccountController(MeFitDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the Account in the database by Id parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadAccountDTO>> GetAccountById(int id)
        {
            var domainAccount = await _context.Accounts.FindAsync(id);

            if (domainAccount == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadAccountDTO>(domainAccount);
        }

        /// <summary>
        /// Updates a specified account by id with new data through a update-dto 
        /// </summary>
        /// <param name="updateAccount"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateAccount([FromBody] UpdateAccountDTO updateAccount)
        {
            var domainAccount = _mapper.Map<Account>(updateAccount);

            _context.Entry(domainAccount).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Adds a specified planId by int into the accounts foreign planId key
        /// </summary>
        /// <param name="id"></param>
        /// <param name="PlanID"></param>
        /// <returns></returns>
        // PUT api/<ExerciseController>/5
        [HttpPut("{id}/updatePlanId/{PlanID}")]
        public async Task<ActionResult> PutPlanId(int id, int PlanID)
        {
            var domainAccount = _context.Accounts.Find(id);

            if (domainAccount == null)
            {
                return BadRequest();
            }
            else
            {
                domainAccount.PlanID = PlanID;
            }

            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Adds a new account to the database by supplying new data through a create-dto
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddNewAccount([FromBody] CreateAccountDTO account)
        {
            var domainAccount = _mapper.Map<Account>(account);
            _context.Accounts.Add(domainAccount);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        /// <summary>
        /// Deletes a account specified ny id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAccount(int id)
        {
            var account = _context.Accounts.Find(id);
            if (account == null)
            {
                return NotFound();
            }

            _context.Accounts.Remove(account);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
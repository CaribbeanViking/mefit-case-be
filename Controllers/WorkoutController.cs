﻿using AutoMapper;
using mefit_case_be.Data;
using mefit_case_be.DTO.ExerciseDTOs;
using mefit_case_be.DTO.WorkoutDTOs;
using mefit_case_be.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace mefit_case_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkoutController : ControllerBase
    {
        // Dependency Injections
        private readonly MeFitDBContext _context;
        private readonly IMapper _mapper;
        public WorkoutController(MeFitDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

                    // ----------------------------------------------------------------------------------- //
        // -------------------------------------- // CONTROLLERS/ENDPOINTS \\ ------------------------------------------ //
                    // ----------------------------------------------------------------------------------- //

        // Delete workout, add, patch exercises

        // GET: api/<WorkoutController>
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<ReadWorkoutDTO>>> GetAllWorkouts()
        {
            return _mapper.Map<List<ReadWorkoutDTO>>(await _context.Workouts
                .Include(p => p.Exercises)
                .ToListAsync());
        }
        // GET api/<PlanController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadWorkoutDTO>> GetWorkoutById(int id)
        {
            Workout workout = await _context.Workouts
                .Include(p => p.Exercises)
                .Where(p => p.Id == id)
                .FirstAsync();

            if (workout == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadWorkoutDTO>(workout);
        }
        [HttpPatch("{id}")]
        public async Task<ActionResult> PatchWorkoutById(int id, [FromBody] UpdateWorkoutDTO workout)
        {  
            var domainWorkout = _mapper.Map<Workout>(workout);
            _context.Entry(domainWorkout).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return NoContent();
        }
        [HttpPost]
        public async Task<ActionResult> AddWorkout([FromBody] CreateWorkoutDTO workout)
        {
            var domainWorkout = _mapper.Map<Workout>(workout);
            _context.Workouts.Add(domainWorkout);
            await _context.SaveChangesAsync();
            return Ok();
        }
        [HttpGet("{id}/Exercises/all")]
        public async Task<ActionResult<List<ReadExerciseDTO>>> GetExercisesByWorkoutId(int id)
        {
            return _mapper.Map<List<ReadExerciseDTO>>(await _context.Exercises
                .Where(m => m.Workouts.Select(w => w.Id).Contains(id))
                .ToListAsync());
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteWorkoutById(int id)
        {
            var workout = _context.Workouts.Find(id);
            if (workout == null)
            {
                return NotFound();
            }

            _context.Workouts.Remove(workout);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}

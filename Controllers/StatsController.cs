﻿using AutoMapper;
using mefit_case_be.Data;
using mefit_case_be.DTO.StatsDTOs;
using mefit_case_be.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace mefit_case_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatsController : ControllerBase
    {
        // Dependency Injections
        private readonly MeFitDBContext _context;
        private readonly IMapper _mapper;
        public StatsController(MeFitDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/<StatsController>
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<ReadStatsDTO>>> GetAllStats()
        {
            return _mapper.Map<List<ReadStatsDTO>>(await _context.Stats
                .Include(p => p.Plans)
                .ToListAsync());
        }

        // GET api/<StatsController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadStatsDTO>> GetStatsById(int id)
        {
            Stats stats = await _context.Stats
                .Include(p => p.Plans)
                .Where(p => p.Id == id)
                .FirstAsync();

            if (stats == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadStatsDTO>(stats);
        }

        //[HttpGet("{id}/plans")]
        //public async Task<ActionResult<List<ReadStatsDTO>>> GetPlansInStatsById(int id)
        //{
        //    // A Character will be displayed including the Movies that have the same id as the input id.

        //    return _mapper.Map<List<ReadStatsDTO>>(await _context.Stats
        //        .Where(c => c.Plans.
        //}

        // POST api/<StatsController>
        [HttpPost]
        public async Task<ActionResult> AddStats([FromBody] CreateStatsDTO stats)
        {
            var domainStat = _mapper.Map<Stats>(stats);
            _context.Stats.Add(domainStat);
            await _context.SaveChangesAsync();
            return Ok();
        }


        // ----------------------------------------------- //

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateStats([FromBody] UpdateStatsDTO updateStats)
        {
            var domainStats = _mapper.Map<Stats>(updateStats);

            _context.Entry(domainStats).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // ----------------------------------------------- //

        /// <summary>
        /// Adds a new planId into the list of plans for a specified Stats.
        /// <para>Duplicated the existing list of plans into a temporary list and then adds the new plan.</para>
        /// <para>Updates the current plan list with the old and new plans.</para>
        /// </summary>
        /// <param name="id"></param>
        /// <param name="PlanId"></param>
        /// <returns></returns>
        // PUT api/<StatsController>/5
        [HttpPut("{id}/plans/{PlanId}")]
        public async Task<ActionResult> PutStatsCompletedPlan(int id, int PlanId)
        {
            //var domainStats = _context.Stats.Find(id);

            Stats statsTest = await _context.Stats
                .Where(m => m.Id == id)
                .FirstAsync();

            var domainPlan = await _context.Plans.FindAsync(PlanId);

            List<Plan> plans = new();

            if (statsTest.Plans != null)
            {
                foreach (var item in statsTest.Plans)
                {
                    plans.Add(item);
                }
            }

            plans.Add(domainPlan);

            statsTest.Plans = plans;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Resets the bools in for the specified Stats by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // PUT api/<StatsController>/5
        [HttpPut("{id}/reset")]
        public async Task<ActionResult> PutStatsHardReset(int id)
        {
            var domainStats = _context.Stats.Find(id);

            domainStats.Button_1 = false;
            domainStats.Button_2 = false;
            domainStats.Button_3 = false;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        // DELETE api/<StatsController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteStatsById(int id)
        {
            var stats = _context.Stats.Find(id);
            if (stats == null)
            {
                return NotFound();
            }

            _context.Stats.Remove(stats);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}

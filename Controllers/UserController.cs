﻿using AutoMapper;
using mefit_case_be.Data;
using mefit_case_be.DTO.UserDTOs;
using mefit_case_be.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace mefit_case_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        // Dependency Injections
        private readonly MeFitDBContext _context;
        private readonly IMapper _mapper;

        // Constructor
        public UserController(MeFitDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
                                           // ---------------------------------------- //
                        // ----------------------------------------------------------------------------------- //
        // -------------------------------------- // CONTROLLERS/ENDPOINTS \\ ------------------------------------------ //
                        // ----------------------------------------------------------------------------------- //
                                           // ---------------------------------------- //

        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<ReadUserDTO>>> GetAllUsers()
        {
            return _mapper.Map<List<ReadUserDTO>>(await _context.Users.ToListAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ReadUserDTO>> GetUserById(int id)
        {
            var domainUser = await _context.Users.FindAsync(id);

            if (domainUser == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadUserDTO>(domainUser);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateUser([FromBody] UpdateUserDTO updateUser)
        {
            var domainUser = _mapper.Map<User>(updateUser);

            _context.Entry(domainUser).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // POST api/<PlanController>/5
        [HttpPost]
        public async Task<ActionResult> AddNewUser([FromBody] CreateUserDTO user)
        {
            var domainUser = _mapper.Map<User>(user);
            _context.Users.Add(domainUser);
            await _context.SaveChangesAsync();
            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteUser(int id)
        {
            var user = _context.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}


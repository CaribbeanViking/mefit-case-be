﻿using AutoMapper;
using mefit_case_be.Data;
using mefit_case_be.DTO.ExerciseDTOs;
using mefit_case_be.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace mefit_case_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExerciseController : ControllerBase
    {
        // Dependency Injections
        private readonly MeFitDBContext _context;
        private readonly IMapper _mapper;

        // Constructor
        public ExerciseController(MeFitDBContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
                                           // ---------------------------------------- \\
                     // ----------------------------------------------------------------------------------- \\
        // -------------------------------------- // CONTROLLERS/ENDPOINTS \\ ------------------------------------------ \\
                    // ----------------------------------------------------------------------------------- \\
                                           // ---------------------------------------- \\


        // GET: api/<ExerciseController>
        [HttpGet("all")]
        public async Task<ActionResult<IEnumerable<ReadExerciseDTO>>> GetAllExercises()
        {
            return _mapper.Map<List<ReadExerciseDTO>>(await _context.Exercises
                .Include(e => e.Workouts)
                .ToListAsync());
        }

        // GET api/<ExerciseController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadExerciseDTO>> GetExerciseById(int id)
        {
            var domainExercise = await _context.Exercises
                .Include(e => e.Workouts)
                .Where(e => e.Id == id)
                .FirstAsync();

            if (domainExercise == null)
            {
                return NotFound();
            }

            return _mapper.Map<ReadExerciseDTO>(domainExercise);
        }

        // POST api/<ExerciseController>
        [HttpPost]
        public async Task<ActionResult> PostNewExercise([FromBody] CreateExerciseDTO exercise)
        {
            var newExercise = _mapper.Map<Exercise>(exercise);
            _context.Exercises.Add(newExercise);
            await _context.SaveChangesAsync();

            var readExerciseDTO = _mapper.Map<ReadExerciseDTO>(newExercise);

            return CreatedAtAction("GetExerciseById", new { id = newExercise.Id }, readExerciseDTO);
        }

        // PUT api/<ExerciseController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> PutExercise(int id, [FromBody] UpdateExerciseDTO updateExercise)
        {
            var domainExercise = _mapper.Map<Exercise>(updateExercise);

            _context.Entry(domainExercise).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// This endpoint is deprecated due to a re-structure in backend and functionality
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // PUT api/<ExerciseController>/5
        [HttpPut("{id}/is_complete")]
        public async Task<ActionResult> PutExerciseIsComplete(int id)
        {
            var domainExercise = _context.Exercises.Find(id);

            if (domainExercise.Is_Complete == false)
            {
                domainExercise.Is_Complete = true;
            }
            else
            {
                domainExercise.Is_Complete = false;
            }

            await _context.SaveChangesAsync();

            return NoContent();
        }

        // PUT api/<ExerciseController>/5
        [HttpPut("{id}/workouts/all")]
        public async Task<ActionResult> UpdateExerciseWorkouts(int id, List<int> workouts)
        {
            Exercise exerciseToUpdateWorkouts = await _context.Exercises
                .Include(e => e.Workouts)
                .Where(e => e.Id == id)
                .FirstAsync();

            List<Workout> workout = new();
            foreach (int workoutId in workouts)
            {
                Workout temp = await _context.Workouts.FindAsync(workoutId);
                if (temp == null)
                {
                    return BadRequest("Workout does not exist!");
                }

                workout.Add(temp);
            }

            exerciseToUpdateWorkouts.Workouts = workout;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        // DELETE api/<ExerciseController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteExerciseById(int id)
        {
            var exercise = _context.Exercises.Find(id);
            if (exercise == null)
            {
                return NotFound();
            }

            _context.Exercises.Remove(exercise);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}

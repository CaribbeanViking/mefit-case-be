## Release
Version: 1.0
Date: 2022-09-21

## Description
This is a workout web API created using ASP.NET. It allows users to create new users, update user info, select a workout scheme(plan) and complete workouts. The user can also view their completed plans. The API supplies several endpoints with CRUD-operations as well as some other GET/UPDATE functionality.

## Installation

To run locally, SSMS is needed along with Microsoft EntityFrameworkCore-; SQL Server, Tools, Design, DependencyInjection in Visual Studio. Other packages such as AutoMapper, AutoMapper extentions microsoft DependencyInjection. Swashbuckle.AspNetCore is also needed but should come with the cloning.
In order to migrate the models to a database either SQL Manager Studio is required or a deployed server.

## Using the application

Run Program.cs and the local SSMS application. Swagger should be launched automatically which allows a user to use all endpoints.

## Help

Thouroghly commented for assistance using summaries. Documentation (summaries) is available in the controllers, but are not visible in swagger.

## Contributors

https://gitlab.com/CaribbeanViking
https://gitlab.com/saintgod
https://gitlab.com/oskar.nyman

## Contributing

No one else than above mentioned.

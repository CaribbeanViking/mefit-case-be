﻿namespace mefit_case_be.DTO.WorkoutDTOs
{
    public class UpdateWorkoutDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Is_Complete { get; set; }
    }
}

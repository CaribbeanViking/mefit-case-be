﻿using mefit_case_be.Model;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace mefit_case_be.DTO.WorkoutDTOs
{
    public class ReadWorkoutDTO
    {
       
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Is_Complete { get; set; }

        // Collection of Programs and Exercises to represent Many to Many relationship
        //public ICollection<Plan> Plans { get; set; }
        public List<int> Exercises { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace mefit_case_be.DTO.WorkoutDTOs
{
    public class CreateWorkoutDTO
    {
        [Required, MaxLength(50)]
        public string Name { get; set; }
        [Required, MaxLength(500)]
        public string Description { get; set; }
        [Required]
        public bool Is_Complete { get; set; }
        //public List<int> Exercises { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace mefit_case_be.DTO.ExerciseDTOs
{
    public class UpdateExerciseDTO
    {
        public int Id { get; set; }
        [Required, MaxLength(50)]
        public string Name { get; set; }
        [Required, MaxLength(500)]
        public string Description { get; set; }

        public bool Is_Complete { get; set; }
    }
}

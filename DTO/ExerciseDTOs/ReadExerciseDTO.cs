﻿namespace mefit_case_be.DTO.ExerciseDTOs
{
    public class ReadExerciseDTO
    {
        public int Id { get; set; }
        
        public string Name { get; set; }
        
        public string Description { get; set; }
        
        public bool Is_Complete { get; set; }

        // List of workout ids that have a relationship with this exercise
        public List<int> Workouts { get; set; }
    }
}

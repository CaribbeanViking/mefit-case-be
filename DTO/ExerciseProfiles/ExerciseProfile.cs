﻿using AutoMapper;
using mefit_case_be.DTO.ExerciseDTOs;
using mefit_case_be.Model;

namespace mefit_case_be.DTO.ExerciseProfiles
{
    public class ExerciseProfile : AutoMapper.Profile
    {
        public ExerciseProfile()
        {
            // Maps selected workout ids to the list of workouts that have a realtionship with this exercise
            CreateMap<Exercise, ReadExerciseDTO>()
                .ForMember(rdto => rdto.Workouts, opt => opt
                    .MapFrom(e => e.Workouts
                    .Select(w => w.Id)
                    .ToArray()))
                .ReverseMap();

            CreateMap<Exercise, CreateExerciseDTO>().ReverseMap();
            CreateMap<Exercise, UpdateExerciseDTO>().ReverseMap();
                
        }
    }
}

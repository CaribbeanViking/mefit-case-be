﻿using AutoMapper;
using mefit_case_be.DTO.AddressDTOs;
using mefit_case_be.Model;
namespace mefit_case_be.DTO.AddressProfiles
{
    public class AddressProfile : Profile
    {
        public AddressProfile()
        {
            CreateMap<UpdateAddressDTO, Address>().ReverseMap();
            CreateMap<ReadAddressDTO, Address>().ReverseMap();
            CreateMap<CreateAddressDTO, Address>().ReverseMap();
        }
    }
}
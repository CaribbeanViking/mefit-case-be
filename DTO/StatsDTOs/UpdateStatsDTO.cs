﻿namespace mefit_case_be.DTO.StatsDTOs
{
    public class UpdateStatsDTO
    {
        public int Id { get; set; }
        public bool? Button_1 { get; set; }
        public bool? Button_2 { get; set; }
        public bool? Button_3 { get; set; }
        public bool? Is_Complete { get; set; }
    }
}

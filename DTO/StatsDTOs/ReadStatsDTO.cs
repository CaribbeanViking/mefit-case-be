﻿namespace mefit_case_be.DTO.StatsDTOs
{
    public class ReadStatsDTO
    {
        public int Id { get; set; }
        public bool Button_1 { get; set; }
        public bool Button_2 { get; set; }
        public bool Button_3 { get; set; }
        public bool Is_Complete { get; set; }

        // Collection of Workouts to represent Many to Many relationship
        public List<int> Plans { get; set; }
    }
}

﻿using AutoMapper;
using mefit_case_be.DTO.StatsDTOs;
using mefit_case_be.Model;

namespace mefit_case_be.DTO.StatsProfiles
{
    public class StatsProfile : Profile
    {
        public StatsProfile()
        {
            // Maps Plan Ids to the Plans list of ints that have a realtionship with this Stats
            CreateMap<Stats, ReadStatsDTO > ().ForMember(sdto => sdto.Plans, opt => opt.MapFrom(c => c.Plans.Select(c => c.Id).ToArray())).ReverseMap();

            CreateMap<UpdateStatsDTO, Stats>().ReverseMap();

            CreateMap<Stats, CreateStatsDTO>().ReverseMap();
        }
    }
}

﻿using AutoMapper;
using mefit_case_be.DTO.WorkoutDTOs;
using mefit_case_be.Model;

namespace mefit_case_be.DTO.WorkoutProfiles
{
    public class WorkoutProfile : AutoMapper.Profile
    {
        public WorkoutProfile()
        {
            // Maps Exercise Ids that have a realtionship with this Workout to the Exercuse List of ints
            CreateMap<Workout, ReadWorkoutDTO>().ForMember(cdto => cdto.Exercises, opt => opt.MapFrom(c => c.Exercises.Select(c => c.Id).ToArray())).ReverseMap();
            CreateMap<Workout, UpdateWorkoutDTO>().ReverseMap();
            CreateMap<CreateWorkoutDTO, Workout>().ReverseMap();
        }
    }
}
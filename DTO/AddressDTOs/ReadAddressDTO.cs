﻿using System.ComponentModel.DataAnnotations;
namespace mefit_case_be.DTO.AddressDTOs
{
    public class ReadAddressDTO
    {
        public int Id { get; set; }
        public string Address_1 { get; set; }
        public string Postal_Code { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}

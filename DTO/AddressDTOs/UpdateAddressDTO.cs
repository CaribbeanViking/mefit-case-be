﻿using System.ComponentModel.DataAnnotations;
namespace mefit_case_be.DTO.AddressDTOs
{
    public class UpdateAddressDTO
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string? Address_1 { get; set; }

        [MaxLength(50)]
        public string? Postal_Code { get; set; }
        [MaxLength(100)]
        public string? City { get; set; }
        [MaxLength(100)]
        public string? Country { get; set; }
    }
}

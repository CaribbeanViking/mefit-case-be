﻿namespace mefit_case_be.DTO.AccountDTOs
{
    public class ReadAccountDTO
    {
        public int Id { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public string Medical_Conditions { get; set; }
        public string Disabilities { get; set; }
        public int UserId { get; set; }
        public int AddressId { get; set; }
        public int StatsId { get; set; }
        public int PlanId { get; set; }

    }
}

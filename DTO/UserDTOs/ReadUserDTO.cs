﻿namespace mefit_case_be.DTO.UserDTOs;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public class ReadUserDTO
{
    public int Id { get; set; }

    public string First_Name { get; set; }

    public string Last_Name { get; set; }

    public string Email { get; set; }

    public string Password { get; set; }
    public bool Is_Admin { get; set; }
    public bool Is_Contributor { get; set; }
}
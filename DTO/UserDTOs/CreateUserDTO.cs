﻿namespace mefit_case_be.DTO.UserDTOs;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

public class CreateUserDTO
{
    [Required, MaxLength(100)]
    public string? Email { get; set; }
    [Required, MaxLength(100)]
    public string? Password { get; set; }
}
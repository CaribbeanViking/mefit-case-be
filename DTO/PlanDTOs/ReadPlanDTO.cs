﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mefit_case_be.DTO.PlanDTOs
{
    public class ReadPlanDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int WorkOutsLeft { get; set; }
        public bool Is_Complete { get; set; }

        // Collection of Workouts to represent Many to Many relationship
        public List<int> Workouts { get; set; }
    }
}

﻿namespace mefit_case_be.DTO.PlanDTOs
{
    public class UpdatePlanDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int? WorkOutsLeft { get; set; }
        public bool? Is_Complete { get; set; }
    }
}

﻿using AutoMapper;
using mefit_case_be.DTO.UserDTOs;
using mefit_case_be.Model;
namespace mefit_case_be.DTO.UserProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<CreateUserDTO, User>().ReverseMap();
            CreateMap<ReadUserDTO, User>().ReverseMap();
            CreateMap<UpdateUserDTO, User>().ReverseMap();
        }
    }
}

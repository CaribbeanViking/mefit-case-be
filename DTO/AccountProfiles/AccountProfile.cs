﻿using AutoMapper;
using mefit_case_be.DTO.AccountDTOs;
using mefit_case_be.Model;
namespace mefit_case_be.DTO.AccountProfiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<UpdateAccountDTO, Account>().ReverseMap();
            CreateMap<ReadAccountDTO, Account>().ReverseMap();

            // Maps UserId, AddressId and StatsId to the created account, these are all foreign keys.
            CreateMap<Account, CreateAccountDTO>().ForMember(a => a.UserID, opt => opt
                    .MapFrom(a => a.UserID)).ForMember(a => a.AddressID, opt => opt
                    .MapFrom(a => a.AddressID)).ForMember(a=> a.StatsID, opt=> opt
                    .MapFrom(a=>a.StatsID)).ReverseMap();
        }
    }
}
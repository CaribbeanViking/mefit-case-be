﻿using mefit_case_be.DTO.PlanDTOs;
using mefit_case_be.Model;
using AutoMapper;

namespace mefit_case_be.DTO.PlanProfiles
{
    public class PlanProfile : AutoMapper.Profile
    {
        public PlanProfile()
        {
            // Maps workout Ids for every workout that has a realtionship with this exercise, adds the int to the Workouts List(int)
            CreateMap<Plan, ReadPlanDTO>()
                .ForMember(pdto => pdto.Workouts, opt => opt
                    .MapFrom(p => p.Workouts
                    .Select(p => p.Id)
                    .ToArray()))
                .ReverseMap();

            CreateMap<Plan, CreatePlanDTO>().ReverseMap();

            CreateMap<Plan, UpdatePlanDTO>()
                //.ForMember(p => p.Id, opt => opt
                //    .MapFrom(p => p.Id))
                //.ForMember(p => p.Name, opt => opt
                //    .Ignore())
                //.ForMember(p => p.Description, opt => opt
                //    .Ignore())
                //.ForMember(p => p.WorkOutsLeft, opt => opt
                //    .Ignore())
                //.ForMember(p => p.Is_Complete, opt => opt
                //    .MapFrom(p => p.Is_Complete))
                .ReverseMap();
                
                
                    
                //.ForMember(pdto => pdto.Description, opt => opt
                //    .MapFrom(p => p.Description))
                //.ForMember(pdto => pdto.WorkOutsLeft, opt => opt
                //    .MapFrom(p => p.WorkOutsLeft))
        }
    }
}

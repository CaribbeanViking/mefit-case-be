﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mefit_case_be.Model
{
    [Table("Workout")]
    public class Workout
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
       
        public string Name { get; set; }
        [Required, MaxLength(500)]
        public string Description { get; set; }
        [Required]
        public bool Is_Complete { get; set; }

        // Collection of Programs and Exercises to represent Many to Many relationship
        public ICollection<Plan> Plans { get; set; }
        public ICollection<Exercise> Exercises { get; set; }
    }
}

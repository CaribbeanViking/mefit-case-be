﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mefit_case_be.Model
{
    [Table("User")]
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [MaxLength(50)]
        public string? First_Name { get; set; }
        [MaxLength(100)]
        public string? Last_Name { get; set; }
        [Required, MaxLength(100)]
        public string? Email { get; set; }
        [MaxLength(100)]
        public string? Password { get; set; }
        public bool? Is_Admin { get; set; }
        public bool? Is_Contributor { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mefit_case_be.Model
{
    [Table("Plan")]
    public class Plan
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required, MaxLength(50)]
        public string? Name { get; set; }
        [MaxLength(500)]
        public string? Description { get; set; }
        public int? WorkOutsLeft { get; set; }
        public bool? Is_Complete { get; set; }

        // Collection of Workouts to represent Many to Many relationship
        public ICollection<Workout>? Workouts { get; set; }

        // Towards the end of the project we realised that a many to many relationship was not established between plans and stats, 
        // which creates a issue with the amount of users that are allowed to be created before a cap is hit.
        // Unforetunately we did not have the time or felt that we could jeopordize the project so close to dealdine by re-structuring this relationship.
    }
}

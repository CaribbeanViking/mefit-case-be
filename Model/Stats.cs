﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mefit_case_be.Model
{
    [Table("Stats")]
    public class Stats
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public bool? Button_1 { get; set; }
        public bool? Button_2 { get; set; }
        public bool? Button_3 { get; set; }
        public bool? Is_Complete { get; set; }
    
        // Collection of Workouts to represent Many to Many relationship
        public ICollection<Plan>? Plans { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mefit_case_be.Model
{
    [Table("Account")]
    public class Account
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string? Weight { get; set; }
        public string? Height { get; set; }
        [MaxLength(200)]
        public string? Medical_Conditions { get; set; }
        [MaxLength(300)]
        public string? Disabilities { get; set; }

        // ---------- Foreign Keys ---------- //
        public int? UserID { get; set; }
        public User? User { get; set; }
        public int? AddressID { get; set; }
        public Address? Address { get; set; }
        public int? StatsID { get; set; }
        public Stats? Stats { get; set; }
        public int? PlanID { get; set; }
        public Plan? Plan { get; set; }
    }
}
